create table nome_questionario(
id int not null auto_increment,
nome varchar(30),
primary key(id)
);

create table questao_questionario(
numero int not null auto_increment,
questao varchar(30),
resposta enum('V','F'),
id_nome int, 
primary key(numero),
foreign key(id_nome) references nome_questionario(id)

);

alter table questao_questionario change numero id int not null auto_increment;

select nome from nome_questionario;

select q.questao,q.resposta, n.nome from questao_questionario as q join nome_questionario as n
on n.id = q.id_nome
where n.nome='Teste de Questionário';
