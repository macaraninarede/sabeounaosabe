<?php
	session_start();
	include 'includes/conexao_banco.php';
	if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:../index.php');
	  }
		
		mysqli_set_charset($connect,"utf8");
	  $login = $_SESSION['email'];
	  $query_select_login = "SELECT * FROM usuario WHERE email ='$login' ";
	  $select_login = mysqli_query($connect,$query_select_login);
		$array_login = mysqli_fetch_assoc($select_login);

	if($array_login['tipo_conta'] == "professor"){
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<title>Cadastro de Questionário</title>
<link rel="stylesheet" href="css/style_questionario.css">

<!--INCORPORANDO MATERIAL-ICONS-->
<link href="material-icons/iconfont/material-icons.css" rel="stylesheet">

</head>

<body>
	<form action="includes/questionario_inserir.php" method="GET">
		<div class="wrapper">
			
				<div class="card">
						<i class="material-icons md-dark align-center" alt="Avatar" style="width: 100%">account_circle</i>
						<div class="container">
							<h4 class="align-center"><b><?php echo $array_login['nome'];?></b></h4>
							<p class="align-center">Seja bem Vindo!</p>
						</div>
				</div>

			<div class="input-group align-center">
				<select class="basic" name="area">
						<option value="">Grande Área</option>
						<option value="Saúde">Saúde</option>
						<option value="Exatas">Exatas</option>
						<option value="Humanas">Humanas</option>
				</select>
			</div>

			<div class="input-group align-center">
				<select class="basic" name="curso">
						<option value="">Curso</option>
						<option value="Sistemas de Informação">Sistemas de Informação</option>
						<option value="Análise de Sistemas">Análise de Sistemas</option>
						<option value="Ciência da Computação">Ciência da Computação</option>
						<option value="Direito">Direito</option>
						<option value="Engenharia Civil">Engenharia Civil</option>
						<option value="Engenharia Elétrica">Engenharia Elétrica</option>
						<option value="Engenharia Alimentícia">Engenharia Alimentícia</option>
						<option value="Biologia">Biologia</option>
						<option value="Química">Química</option>
						<option value="Medicina">Medicina</option>
						<option value="Artes">Artes</option>
						<option value="Ciências Contábeis">Ciências Contábeis</option>
						<option value="Educação Físic">Educação Física</option>
						<option value="Gastronomia">Gastronomia</option>
						<option value="Física">Física</option>
						<option value="Matemática">Matemática</option>
						<option value="Outro">Outro</option>
				</select>
			</div>

			<div class="input-group align-center">
				<input type="text" name="Disciplina" required>
				<label>Disciplina</label>
			</div>
			
			<div class="input-group align-center">
				<input type="text" name="titulo" required>
				<label>Título do Questionário</label>
			</div>
			
			<div class="input-group">
				<input class="questao" type="text" name="questao" required>
				<label>Digite a questão</label>
			</div>

			<div class="input-group">
					<table border="0">
						<tr>
								<th>Verdadeiro</td>
								<th>Falso</td>
						</tr>
						<tr>
								<td><input id="radio" value="V" name="tipo_conta" type="radio"></td>
								<td><input id="radio" value="F" name="tipo_conta" type="radio"></td>
						</tr>
					</table>
			</div>

			<a id="botao1" href="includes/view_professor.php">Voltar</a>
			<input type="submit" value="Próxima" id="enviar">
			<a id="botao" href="questionario.php">Finalizar</a>
			<a id="botao2" href="includes/view_questionario.php">Questionários</a>
			
		</div>
		<input type="hidden" name="login" value="<?php echo $login;?>">
	</form>
	
</body>

</html>
<?php
}else{
    header('location:index.html');
}
?>