[Bastardos Ingórios]()
================
[Website]() |
[Twitter]() |
[Facebook]() |
### [Developers]()<br>
[Adson Carlos](https://www.facebook.com/adson.carlos.5) | [Keka Lopo](https://www.facebook.com/keka.lopo) | [Luã Gama](http://www.facebook.com/lua.gama) | [Luiz Gouvea](https://www.facebook.com/luiizgouvea)  

**Bastardos The Educational Game** é um projeto opensource, que está sendo desenvolvido como avaliação para o 7º semestre do Curso de Sistemas de Informação da FTC de Vitória da Conquista.

#### Dependências
A aplicação é escrita com tecnologias WEB como PHP, HTML5, JavaScript e CSS. Logo precisamos de um servidor WEB Apache ou NginX com capacidade de interpretar PHP, precisamos também de um banco de dados relacional, recomendamos o MySQL ou MariaDB.
Para fins de estudo, recomendamos o uso do [XAMPP](https://www.apachefriends.org/pt_br/index.html).
#### Como Instalar?

- **Instale o XAMPP (Linux)**
    - Baixar o pacote
    ```bash
        wget https://downloadsapachefriends.global.ssl.fastly.net/7.3.4/xampp-linux-x64-7.3.4-0-installer.run?from_af=true -o xampp.run

    ```
    - Dar Permissão
    ```bash
        sudo chmod +x xampp.run
    ```
    - Instalar
    ```bash
        sudo ./xampp.run
    ```
- **Clone o repositório git:**

```bash
#Via SSH
git@gitlab.com:macaraninarede/te.git
```
```bash
#Via HTTPS
wget https://gitlab.com/macaraninarede/te.git
```

Colocar os arquivos dentro da pasta respectiva do servidor WEB preferido.