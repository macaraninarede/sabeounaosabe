CREATE DATABASE b_game;

USE b_game;
CREATE TABLE usuario(

codigo int auto_increment,
email varchar(30) not null,
senha varchar(50) not null,
tipo_conta varchar(10) not null,
nome varchar(30) not null,
cpf varchar(11),
curriculo varchar(80),
instituicao varchar(30),
titulacao varchar(30),
primary key (codigo)
) DEFAULT charset = utf8;

create table nome_questionario(
id int not null auto_increment,
id_dono int,
area varchar(30),
curso varchar(30),
disciplina varchar(30),
nome varchar(30),
primary key(id)
) DEFAULT charset = utf8;

create table questao_questionario(
id int not null auto_increment,
questao varchar(30),
resposta enum('V','F'),
id_nome int, 
primary key(id),
foreign key(id_nome) references nome_questionario(id)

) DEFAULT charset = utf8;

CREATE TABLE tolken_questionario(
    tolken varchar(30) not null,
    id_nome int,
    PRIMARY KEY(tolken),
    FOREIGN key(id_nome) references nome_questionario(id)
) DEFAULT charset = utf8;

create table aluno_resposta(
id int not null auto_increment,
nome_aluno varchar(30),
questao varchar(30),
resposta enum('V','F'),
primary key(id)
) DEFAULT charset = utf8;

create table aluno_resposta_auditoria(
id int not null auto_increment,
nome_aluno varchar(30),
data_audit date,
questao varchar(30),
resposta enum('V','F'),
primary key(id)
) DEFAULT charset = utf8;

create table aluno_pontuacao(
id int not null auto_increment,
nome_aluno varchar(30),
id_questao varchar(30),
pontuacao int,
primary key(id)
) DEFAULT charset = utf8;

create table ranking_geral(
    id int not NULL auto_increment,
    nome_questionario varchar(30) not null,
    pontuacao_geral int,
    data_ranking date,
    nome_aluno varchar(30) not null,
    PRIMARY KEY(id)

) DEFAULT charset = utf8;

create table ranking_parcial(
    id int not NULL auto_increment,
    pontuacao int,
    nome_aluno varchar(30) not null,
    PRIMARY KEY(id)

) DEFAULT charset = utf8;


create table sessao(
    id int not NULL auto_increment,
    id_questionario int,
    data_sessao date,
    tipo_conta varchar(30),
    id_aluno varchar(50),
    PRIMARY KEY(id)

) DEFAULT charset = utf8;

create table temporizador(
    id int not NULL auto_increment,
    tempo int,
    PRIMARY KEY(id)

) DEFAULT charset = utf8;

select * from usuario;
