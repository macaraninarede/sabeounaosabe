<?php
    session_start();
    $temp = $_SESSION['email'];
    include 'conexao_banco.php';
    mysqli_set_charset($connect,"utf8");
    $query_select = "SELECT * FROM usuario WHERE email='$temp'";
    $select = mysqli_query($connect,$query_select);
    $array = mysqli_fetch_assoc($select);

    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:../index.php');
      }

if($array['tipo_conta'] == 'professor'){  
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<title>Configurações</title>
<link rel="stylesheet" href="../css/style_configuracoes.css">

<!--INCORPORANDO MATERIAL-ICONS-->
<link href="../material-icons/iconfont/material-icons.css" rel="stylesheet">

</head>

<body>
 
        <div class="wrapper">
            <h1 align="center">Configurações</h1>

                    <form action="alter_conf.php">       
                        <div class="card">
                            
                                    <i class="material-icons md-dark align-center" alt="Avatar" style="width: 100%">account_circle</i>
                                    <div class="container">
                                        <h4 class="align-center"><b> <?php echo $array['nome'];?></b></h4>
                                    </div>
                        </div>

                        <div class="descricao">
                        <h4>Tempo para cada pergunta</h4>
                        </div>

                        <div class="input-group temporizador">
                            <input class="segundos"  type="text" name="segundos" >
                            <label>Segundos</label>
                        </div>
                        
                        <div class="descricao">
                        <h4>Tempo para cada pergunta</h4>
                        </div>

                        <div class="input-group">
                            <input class="questao"  type="password" name="senha_atual">
                            <label>Senha Atual</label>
                        </div>
                        
                        <div class="input-group">
                            <input class="questao"  type="password" name="nova_senha" >
                            <label>Nova Senha</label>
                        </div>
                        
                        <div class="input-group">
                            <input class="questao"  type="password" name="confirma_senha" >
                            <label>Confirme a nova senha</label>
				        </div>

                        <div class="input-group">
                            <input class="a" type="submit" value="Finalizar" id="enviar">
                            <!--<a  href="#">Finalizar</a>-->    
                            <a  href="view_professor.php">Voltar</a>
                        </div>

                        <input class="segundos"  type="hidden" name="login" value="<?php echo $temp;?>">
                    </form>
          </div>
     
</body>

</html>
<?php
}else{
    header('location:../index.html');
}
?>