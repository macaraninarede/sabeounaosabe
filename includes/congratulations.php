<?php

include 'conexao_banco.php';
	 
	 mysqli_set_charset($connect,"utf8");
	 $query_select = "SELECT * FROM ranking_geral ORDER BY pontuacao_geral DESC LIMIT 5";
	 $select = mysqli_query($connect,$query_select);
	 $array = mysqli_fetch_assoc($select);
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<title>Ranking</title>
<link rel="stylesheet" href="../css/style_congratulations.css">
<script language="javascript" src="../js/moto.js"></script>

<!--INCORPORANDO MATERIAL-ICONS-->
<link href="../material-icons/iconfont/material-icons.css" rel="stylesheet">

</head>

<body>
	<div class="corpo">	

	
	<?php
		$num = 1;
		do{
			
	?>
			<div class="card">
				<i class="material-icons md-dark align-center" alt="Avatar" style="width: 100%">account_circle</i>
				<div class="container">
					<h4 class="align-center"><b><?php echo $array['nome_aluno'];?></b></h4>
					<h4 class="align-center"><b><?php echo $num;?>ª Colocação</b></h4>
					<p class="align-center"><?php echo $array['pontuacao_geral'];?> Pontos</p>
				</div>
			</div>
	<?php
		$num ++;
		}while($array = mysqli_fetch_assoc($select));
	?>
		<a class="sair" href="../index.html">Sair</a>
	</div>
</body>

</html>
