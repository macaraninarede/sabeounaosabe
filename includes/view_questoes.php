<?php

    session_start();

    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
    unset($_SESSION['login']);
    unset($_SESSION['senha']);
    header('location:../index.php');
    }
    

    if(empty($_GET['temp'])){
        header('location:view_questionario.php');
    }else{
    $titulo = $_GET['temp'];
    }

	include 'conexao_banco.php';
    mysqli_set_charset($connect,"utf8");
    $query_select = "SELECT q.questao,q.resposta FROM questao_questionario AS q JOIN nome_questionario AS n ON n.id = q.id_nome WHERE n.nome='$titulo'";

    $select = mysqli_query($connect,$query_select);
    $array = mysqli_fetch_assoc($select);
    $total = mysqli_num_rows($select);

   
    $login = $_SESSION['email'];
    $query_select_login = "SELECT * FROM usuario WHERE email ='$login' ";
    $select_login = mysqli_query($connect,$query_select_login);
    $array_login = mysqli_fetch_assoc($select_login);
  if($array_login['tipo_conta'] == "professor"){
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<title>Questões</title>
<link rel="stylesheet" href="../css/style_questao.css">

<!--INCORPORANDO MATERIAL-ICONS-->
<link href="../material-icons/iconfont/material-icons.css" rel="stylesheet">

</head>

<body>
    <?php
    if($total > 0){
    ?>
    <div class="wrapper">

        <h1 align="center"><?php echo $titulo;?></h1>
        <div class="questoes">
                <?php 
                $numero = 1;   
                do{
                    
                    echo"<div class='quest'>"; 
                        echo "<h3>";
                        echo $numero.' - '.$array['questao'];
                        echo "</h3>";
                    echo"</div>"; 
                    echo"<div class='resp'>";
                        echo "<h6>";
                        echo $array['resposta'];
                        echo "</h6>";
                    echo"</div>";
                    $numero += 1;

                    
                }while($array = mysqli_fetch_assoc($select));
                ?>   
            </div>

            <div class="botoes">
                          <a id="botao" href="view_questionario.php">Voltar</a>
            </div>
            <div class="botoes">
            <?php
            echo '<a id="botao"';
            echo 'href="delete.php?temp='. $titulo .'">'
            ?>
            Remover Questionário</a>
            </div>
    </div>
    <?php
    }else{
    ?>
    <div class="wrapper">
    </div>

    <?php
    }
    ?>
</body>

</html>
<?php
}else{
    header('location:../index.html');
}
?>