<?php
    session_start();

    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
    unset($_SESSION['login']);
    unset($_SESSION['senha']);
    header('location:../index.php');
    }
    
    include 'conexao_banco.php';

    /*Select para pegar os dados da tabela aluno_pontuacao*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT * FROM aluno_pontuacao";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
    $id_questao = $array['id_questao'];

    /*Select para pegar o id do nome do questionario*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT id_nome FROM questao_questionario WHERE id = '$id_questao'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
    $id_nome_questionario = $array['id_nome'];

    /*Select para pegar o nome do questionario*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT nome FROM nome_questionario WHERE id = '$id_nome_questionario'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
    $nome_questionario = $array['nome'];


    /*Select aluno_resposta*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select_resposta = "SELECT * FROM aluno_resposta";
	$select_resposta = mysqli_query($connect,$query_select_resposta);
	$array_resposta = mysqli_fetch_assoc($select_resposta);


    /*Select para pegar os dados da tabela ranking_parcial*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT * FROM ranking_parcial";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
    
    do{
        $id = $array['id'];
        $pontuacao = $array['pontuacao'];
        $nome_aluno = $array['nome_aluno'];
        $datahora= date('Y-m-d h:i:s');

        mysqli_set_charset($connect,"utf8");
	    $query_insert = "INSERT INTO ranking_geral (id, nome_questionario, pontuacao_geral, data_ranking,nome_aluno) VALUES ('$id','$nome_questionario','$pontuacao','$datahora','$nome_aluno')";
	    $insert = mysqli_query($connect,$query_insert);
        
        mysqli_set_charset($connect,"utf8");
	    $query_delete = "DELETE FROM ranking_parcial WHERE id = '$id'";
	    $delete = mysqli_query($connect,$query_delete);
        
        

    }while($array = mysqli_fetch_assoc($select));

    do{
        $id = $array_resposta['id'];
        $nome_aluno = $array_resposta['nome_aluno'];
        $questao = $array_resposta['questao'];
        $resposta = $array_resposta['resposta'];
        $datahora= date('Y-m-d h:i:s');


        mysqli_set_charset($connect,"utf8");
	    $query_insert = "INSERT INTO aluno_resposta_auditoria (id,nome_aluno,data_audit,questao,resposta) VALUES ('$id','$nome_aluno','$datahora','$questao','$resposta')";
	    $insert = mysqli_query($connect,$query_insert);

    }while($array_resposta = mysqli_fetch_assoc($select_resposta));

    mysqli_set_charset($connect,"utf8");
        $query_truncate = "TRUNCATE TABLE aluno_pontuacao";
        $truncate = mysqli_query($connect,$query_truncate);

    mysqli_set_charset($connect,"utf8");
    $query_truncate = "TRUNCATE TABLE aluno_resposta";
    $truncate = mysqli_query($connect,$query_truncate);

    mysqli_set_charset($connect,"utf8");
    $query_truncate = "TRUNCATE TABLE sessao";
    $truncate = mysqli_query($connect,$query_truncate);

    header('location:congratulations.php');
?>