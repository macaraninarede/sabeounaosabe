<?php
/*$query_select = "SELECT tolken FROM tolken_questionario AS t JOIN nome_questionario AS n ON n.id = t.id_nome WHERE n.nome='$titulo'";*/

	session_start();

	if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
	{
	unset($_SESSION['login']);
	unset($_SESSION['senha']);
	header('location:../index.php');
	}

	include 'conexao_banco.php';
	$tolken = $_GET['tolken'];
	$indice = $_GET['indice'];
	$email = $_SESSION['email'];

	/*Select para pegar o nome do aluno*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT nome FROM usuario WHERE email = '$email'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
	$nome = $array['nome'];

	/*Select para pegar linhas respostas*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT * FROM aluno_resposta WHERE nome_aluno = '$nome'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
	$linha_resposta = mysqli_num_rows($select);
	
	/*Select para descobrir o id do titulo do questionario*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT id_nome FROM tolken_questionario WHERE tolken = '$tolken'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
	$id_nome = $array['id_nome'];

	/*Select para descobrir id professor*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT id_dono FROM nome_questionario WHERE id = '$id_nome'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
	$id_prof = $array['id_dono'];
	

	/*Select para descobrir id professor*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT tempo FROM temporizador WHERE id = '$id_prof'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
	$tempo = $array['tempo'];
	
	
	/*Select para pegar as questoes correspondentes ao id anterior*/ 
	mysqli_set_charset($connect,"utf8");
	$query_select = "SELECT questao FROM questao_questionario WHERE id_nome = '$id_nome'";
	$select = mysqli_query($connect,$query_select);
	$array = mysqli_fetch_assoc($select);
	
	$linhas = mysqli_num_rows($select);
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<title>Play Game</title>
<link rel="stylesheet" href="../css/style_play_game.css">

<!--INCORPORANDO MATERIAL-ICONS-->
<link href="../material-icons/iconfont/material-icons.css" rel="stylesheet">

</head>

<body onload="contagem_regressiva()">

	<form name="form_relogio" action="resp_aluno_inserir.php" method="GET">
	
	<input class="indice_atual" readonly="true" type="text" name="indice_atual" value="<?php echo $indice;?>" required>

	<input class="indice_atual" readonly="true" type="text" name="tolken" value="<?php echo $tolken;?>" required>
		
	<div name="wrapper" class="wrapper">
		
			<div class="impede" id="desabilitar">
				<div class="desabilitado"> Desabilitado</div>
			</div>
		
			
				<div class="card">
						<i class="material-icons md-dark align-center" alt="Avatar" style="width: 100%">account_circle</i>
						<div class="container">
							<h4 class="align-center"><b><?php echo $nome;?></b></h4>
							
						</div>
				</div>
			<?php
		
			$questao = array();
			
			do{
				$teste = $array['questao'];
				/*array_unshift($questao,$teste);*/
				array_push($questao,$teste);

			}while($array = mysqli_fetch_assoc($select));
			
			?>
			
				<div class="input-group">
					<input class="questao" readonly="true" type="text" name="questao" value="<?php echo $questao[$indice];?>" required>
					<label>Questão</label>
				</div>

			
			<div class="input-group">
					<table border="0">
						<tr>
								<th>Verdadeiro</th>
								<th>Falso</th>
						</tr>
						<tr>
								<td><input id="radio" value="V" name="resp" type="radio"></td>
								<td><input id="radio2" value="F" name="resp" type="radio"></td>
								<td><input class="radio_hidden" id="radio2" value="no" name="resp" type="radio" checked></td>
						</tr>
					</table>
			</div>

			
			<?php
				
				if($linha_resposta < ($linhas-1)){

					echo '<input type="submit" value="Próxima" id="enviar">';
				
			?>
			<?php
				}else{
					echo '<a id="botao2" href="javascript:{}" onclick="finalizar();">Finalizar</a>';
				}
			?>

			<input type="text" name="nome_aluno" class="nome_aluno" value="<?php echo $nome;?>">
			<input type="text" name="relogio" size="10" class="relogio" readonly="yes"> 
		</div>
		
	</form>
	
				
	
	<script>
		var count = new Number();
		var count = <?php echo $tempo;?>;

		function contagem_regressiva(){

			if ((count - 1) >= 0){
			count -= 1
			if(count == 30){
				document.form_relogio.relogio.style.background = "rgb(236, 40, 40)"
			}
			if (count == 0) {
				count = "Acabou o tempo"
				document.form_relogio.relogio.style.background = "rgb(100, 9, 9)"
												
			}else if(count < 10){
				count = "0"+count
			}

			if(count == "Acabou o tempo"){
				document.getElementById('desabilitar').style.display = "block"
				
				
			}
			document.form_relogio.relogio.value = count
			setTimeout('contagem_regressiva();', 1000)

			}
			
		}

		function finalizar(){
			document.forms.form_relogio.action = "resp_aluno_ultima.php"
			document.forms.form_relogio.submit()
		}
	</script>
	
</body>

</html>
