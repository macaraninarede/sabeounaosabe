<?php
session_start();
	$titulo = $_GET['titulo'];
	$curso = $_GET['curso'];
	$area = $_GET['area'];
	$disciplina = $_GET['disciplina'];
	
	$login = $_SESSION['email'];

	if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:../index.php');
	  }

		include 'conexao_banco.php';
		mysqli_set_charset($connect,"utf8");
	  $query_select_login = "SELECT * FROM usuario WHERE email ='$login' ";
	  $select_login = mysqli_query($connect,$query_select_login);
	  $array_login = mysqli_fetch_assoc($select_login);
	
		if($array_login['tipo_conta'] == "professor"){
			
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<title>Cadastro de Questionário</title>
<link rel="stylesheet" href="../css/style_questionario.css">

<!--INCORPORANDO MATERIAL-ICONS-->
<link href="../material-icons/iconfont/material-icons.css" rel="stylesheet">

</head>

<body>
	<form action="questionario_inserir_php.php" method="GET">
		<div class="wrapper">
			
				<div class="card">
						<i class="material-icons md-dark align-center" alt="Avatar" style="width: 100%">account_circle</i>
						<div class="container">
							<h4 class="align-center"><b><?php echo $array_login['nome'];?></b></h4>
							<p class="align-center">Seja bem Vindo!</p>
						</div>
				</div>
		
			<div class="input-group align-center">
				<input type="text" readonly="true" name="area" value="<?php echo $area;?>" required>
				<label>Grande Área</label>
			</div>

			<div class="input-group align-center">
				<input type="text" readonly="true" name="curso" value="<?php echo $curso;?>" required>
				<label>Curso</label>
			</div>

			<div class="input-group align-center">
				<input type="text" readonly="true" name="Disciplina" value="<?php echo $disciplina;?>" required>
				<label>Disciplina</label>
			</div>

			<div class="input-group align-center">
				<input type="text" readonly="true" name="titulo" value="<?php echo $titulo;?>" required>
				<label>Título do Questionário</label>
			</div>
			
			<div class="input-group">
				<input class="questao" type="text" name="questao" required>
				<label>Digite a questão</label>
			</div>

			<div class="input-group">
					<table border="0">
						<tr>
								<th>Verdadeiro</td>
								<th>Falso</td>
						</tr>
						<tr>
								<td><input id="radio" value="V" name="tipo_conta" type="radio" checked></td>
								<td><input id="radio" value="F" name="tipo_conta" type="radio"></td>
						</tr>
					</table>
			</div>

			<a id="botao1" href="view_professor.php">Voltar</a>
			<input type="submit" value="Próxima" id="enviar">
			<a id="botao" href="../questionario.php">Finalizar</a>
			<a id="botao2" href="../includes/view_questionario.php">Questionários</a>
			
		</div>
	</form>
	
</body>

</html>
<?php
}else{
    header('location:../index.html');
}
?>